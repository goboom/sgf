use nom::alpha;
use nom::IResult;
use std::str;

use types::*;

/// A Collection is a Vector of Gametrees
named!(
    collection<Collection>,
    map!(many1!(ws!(gametree)), Collection::new)
);

/// A Gametree is a wrapper around `Vec<SequenceElement>`
named!(
    gametree<GameTree>,
    map!(delimited!(tag!("("), sequence, tag!(")")), GameTree::new)
);

/// A `Vec<SequenceElement>`, reprensenting a tree of `Nodes`.
named!(
    sequence<Vec<SequenceElement>>,
    ws!(many1!(sequence_element))
);

/// A node in the Gametree.
named!(
    sequence_element<SequenceElement>,
    alt!(
        gametree => { |t| SequenceElement::new_variation(t) } |
        node => { |n| SequenceElement::new_node(n) }
    )
);

/// A node, consisting of one or more properties
named!(node<Node>, ws!(preceded!(tag!(";"), many1!(property))));

/// A property, representing moves, game setup or annotations and the like
named!(
    property<Property>,
    ws!(map_res!(
        do_parse!(id: property_ident >> args: property_args >> (id, args)),
        |(id, args)| Property::try_new(id, args)
    ))
);

/// The name of the property.
named!(
    property_ident<PropertyIdent>,
    map!(map_res!(alpha, str::from_utf8), PropertyIdent::from)
);

named!(
    escaped_string<String>,
    map_res!(
        escaped_transform!(
            is_not!("]\\"),
            '\\',
            alt!(
    tag!("\\") => { |_| &b"\\"[..] } |
    tag!("]") => { |_| &b"]"[..] } |
    tag!("\n") => { |_| &b"\n"[..] }
)
        ),
        String::from_utf8
    )
);
named!(
    maybe_string<String>,
    map!(opt!(escaped_string), |o| Option::unwrap_or_else(
        o,
        String::new
    ))
);

/// Content of the property. The type depends on the `property_ident` and could
/// range from multi-line escaped strings to move coordinates
named!(
    property_args<Vec<String>>,
    many1!(ws!(delimited!(tag!("["), maybe_string, tag!("]"))))
);

pub fn parse_collection(input: &[u8]) -> Result<Collection, SgfParseError> {
    let result = collection(input);
    match result {
        IResult::Done(_, coll) => Ok(coll),
        IResult::Incomplete(_) => Err(SgfParseError::IncompleteInput),
        IResult::Error(_) => Err(SgfParseError::GeneralParseError),
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use test::Bencher;
    use nom::IResult;
    use types::Property::*;
    use types::Color::*;

    #[test]
    fn move_coordinates() {
        let input = b"W[aa]";
        assert_eq!(
            property(&input[..]),
            IResult::Done(&b""[..], Move(White, Coordinate(0, 0)))
        );
    }

    #[test]
    fn uppercase_coordinate_is_invalid() {
        let input = b"W[AA]";
        assert!(property(&input[..]).is_err());
    }

    #[test]
    fn empty_args() {
        let input = b"[]";
        let result = property_args(input);
        assert!(result.is_done());
    }

    #[test]
    fn escaped_comments() {
        let input =
            r#"C[Comments can have escaped [closing brackets \] and still parse correctly]"#;
        let result = property(input.as_bytes());
        assert!(result.is_done());
    }

    #[test]
    fn escaped_escape() {
        let input = r#"C[An escaped escape character '\\' is accepted]"#;
        let result = property(input.as_bytes());
        assert!(result.is_done());
    }

    #[test]
    fn multiline_comments() {
        let input = b"C[A comment can strech \n over multiple lines]";
        let result = property(input);
        assert!(result.is_done());
    }

    #[test]
    fn escaped_linebreaks() {
        let input = b"Strings can have escaped\\\n linebreaks";
        let result = escaped_string(input);
        match result {
            IResult::Done(_, s) => assert_eq!(s, "Strings can have escaped\n linebreaks"),
            _ => panic!("Parsing linebreaks failed"),
        }
    }

    #[test]
    fn empty_move_is_pass() {
        let input = r#"W[]"#;
        match property(input.as_bytes()) {
            IResult::Done(_, result) => assert!(result == Pass(White)),
            _ => panic!("Input did not parse completely"),
        }
    }

    #[test]
    fn simple_collection() {
        let input = b"(;C[test])";
        let result = collection(input);
        assert!(result.is_done());
    }

    #[test]
    fn nested_collection() {
        let input = b"(;C[test](;C[nested comment]))";
        let result = gametree(input);
        assert!(result.is_done());
    }

    #[test]
    fn multi_gametree_collection() {
        let input = b"(;C[test])(;C[nested comment])";
        assert!(parse_collection(input).is_ok());
    }
    #[test]
    fn example1_parses() {
        let content = include_bytes!("../test/example1.sgf");
        let result = collection(content);
        assert!(result.is_done());
    }
    #[test]
    fn example2_parses() {
        let content = include_bytes!("../test/example2.sgf");
        let result = collection(content);
        assert!(result.is_done());
    }
    #[test]
    fn example3_parses() {
        let content = include_bytes!("../test/example3.sgf");
        let result = collection(content);
        assert!(result.is_done());
    }
    #[test]
    fn example4_parses() {
        let content = include_bytes!("../test/example4.sgf");
        let result = collection(content);
        assert!(result.is_done());
    }
    #[test]
    fn example5_parses() {
        let content = include_bytes!("../test/example5.sgf");
        let result = collection(content);
        assert!(result.is_done());
    }

    #[bench]
    fn bench_example1(b: &mut Bencher) {
        let content = include_bytes!("../test/example1.sgf");
        b.iter(|| collection(content));
    }
    #[bench]
    fn bench_example2(b: &mut Bencher) {
        let content = include_bytes!("../test/example2.sgf");
        b.iter(|| collection(content));
    }
    #[bench]
    fn bench_example3(b: &mut Bencher) {
        let content = include_bytes!("../test/example3.sgf");
        b.iter(|| collection(content));
    }
    #[bench]
    fn bench_example4(b: &mut Bencher) {
        let content = include_bytes!("../test/example4.sgf");
        b.iter(|| collection(content));
    }
    #[bench]
    fn bench_example5(b: &mut Bencher) {
        let content = include_bytes!("../test/example5.sgf");
        b.iter(|| collection(content));
    }
}
