extern crate sgf;
use std::env;
use std::fs::File;
use std::io::prelude::*;

fn main() {
    let filename = env::args().nth(1).unwrap();
    let mut file = File::open(filename).unwrap();
    let mut bytes = Vec::new();
    file.read_to_end(&mut bytes).unwrap();
    let result = sgf::parse_collection(&bytes);
    match result {
        Ok(collection) => println!("{:#?}", collection),
        Err(e) => println!("Error reading file: {:?}", e),
    }
}
